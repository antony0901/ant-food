﻿using AntFood.Contracts;
using AntFood.Domain.Services;
using HotChocolate.Types;

namespace AntFood.GraphQLServer.Schema.Types
{
    public class RestaurantType : ObjectType<Contracts.RestaurantType>
    {
        protected override void Configure(IObjectTypeDescriptor<Contracts.RestaurantType> descriptor)
        {
            descriptor.Field(x => x.Id);
            descriptor.Field(x => x.Name);
            descriptor.Field(x => x.Status);
            descriptor.Field(x => x.Tables)
                .Type<ListType<TableType>>()
                .Resolver(async (ctx) =>
                {
                    var tableService = ctx.Service<ITableService>();
                    var restaurantId = ctx.Parent<Contracts.RestaurantType>().Id;
                    return await tableService.GetTablesAsync(restaurantId);
                });
            descriptor.Field(x => x.Menus)
                .Type<ListType<MenuType>>()
                .Resolver(async (ctx) =>
                {
                    var foodService = ctx.Service<IFoodService>();
                    var resturantId = ctx.Parent<Contracts.RestaurantType>().Id;
                    return await foodService.GetMenusAsync(resturantId);
                });
        }
    }
}
