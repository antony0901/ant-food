﻿using AntFood.Contracts;
using AntFood.Domain.Services;
using HotChocolate.Types;

namespace AntFood.GraphQLServer.Schema.Types
{
    public class OrderItemType : ObjectType<Contracts.Types.OrderItemType>
    {
        protected override void Configure(IObjectTypeDescriptor<Contracts.Types.OrderItemType> descriptor)
        {
            descriptor.Field(x => x.FoodId);
            descriptor.Field(x => x.FoodName);
            descriptor.Field(x => x.Quantity);
            descriptor.Field(x => x.UnitPrice);
        }
    }
}
