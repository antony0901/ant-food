﻿using System;
using AntFood.Domain.Services;
using HotChocolate.Types;

namespace AntFood.GraphQLServer.Schema.Types
{
    public class TableType : ObjectType<Contracts.TableType>
    {
        protected override void Configure(IObjectTypeDescriptor<Contracts.TableType> descriptor)
        {
            descriptor.Field(x => x.Id);
            descriptor.Field(x => x.Name);
            descriptor.Field(x => x.Status);
            descriptor.Field(x => x.Capacity);
            descriptor.Field(x => x.Order);
            descriptor.Field(x => x.UnpaidOrder)
                .Type<OrderType>()
                .Resolver(async (ctx) =>
                {
                    var orderService = ctx.Service<IOrderService>();
                    var parent = ctx.Parent<Contracts.TableType>();
                    var order = await orderService.GetUnpaidOrdersByTableAsync(parent.Id);
                    return order;
                });
            descriptor.Field(x => x.PaidOrders)
                .Type<ListType<OrderType>>()
                .Argument("from", x => x.Type<DateTimeType>())
                .Argument("to", x => x.Type<DateTimeType>())
                .Resolver(async (ctx) =>
                {
                    var orderService = ctx.Service<IOrderService>();
                    var parent = ctx.Parent<Contracts.TableType>();
                    var from = ctx.Argument<DateTime?>("from");
                    var to = ctx.Argument<DateTime?>("to");

                    var order = await orderService.GetPaidOrdersByTableIdAsync(parent.Id, from, to);

                    return order;
                });
        }
    }
}
