﻿using AntFood.Contracts;
using AntFood.Domain.Services;
using HotChocolate.Types;

namespace AntFood.GraphQLServer.Schema.Types
{
    public class FoodType : ObjectType<Contracts.FoodType>
    {
        protected override void Configure(IObjectTypeDescriptor<Contracts.FoodType> descriptor)
        {
            descriptor.Field(x => x.Id);
            descriptor.Field(x => x.Name);
            descriptor.Field(x => x.ImageUrl);
        }
    }
}
