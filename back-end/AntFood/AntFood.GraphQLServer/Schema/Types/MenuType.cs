﻿using AntFood.Contracts;
using AntFood.Domain.Services;
using HotChocolate.Types;

namespace AntFood.GraphQLServer.Schema.Types
{
    public class MenuType : ObjectType<Contracts.Types.MenuType>
    {
        protected override void Configure(IObjectTypeDescriptor<Contracts.Types.MenuType> descriptor)
        {
            descriptor.Field(x => x.Foods).Type<ListType<FoodType>>();
            descriptor.Field(x => x.Category);
        }
    }
}
