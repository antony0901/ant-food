﻿using AntFood.Contracts;
using AntFood.Domain.Services;
using HotChocolate.Types;

namespace AntFood.GraphQLServer.Schema.Types
{
    public class OrderType : ObjectType<Contracts.Types.OrderType>
    {
        protected override void Configure(IObjectTypeDescriptor<Contracts.Types.OrderType> descriptor)
        {
            descriptor.Field(x => x.Id);
            descriptor.Field(x => x.CreatedTime).Type<DateTimeType>();
            descriptor.Field(x => x.PaidStatus);
            descriptor.Field(x => x.TotalPrice);
            descriptor.Field(x => x.OrderItems)
                .Type<ListType<OrderItemType>>()
                .Resolver(async (ctx) =>
                {
                    var orderService = ctx.Service<IOrderService>();
                    var parent = ctx.Parent<Contracts.Types.OrderType>();
                    var orderItems = await orderService.GetOrderItemsAsync(parent.Id);

                    return orderItems;
                });
        }
    }
}
