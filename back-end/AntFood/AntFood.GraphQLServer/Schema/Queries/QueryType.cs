﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Threading.Tasks;
using AntFood.Domain.Services;
using AntFood.GraphQLServer.Schema.Mutations;
using AntFood.GraphQLServer.Types;
using AntFood.GraphQLServer.Schema.Types;
using HotChocolate;
using HotChocolate.Configuration;
using HotChocolate.Types;

namespace AntFood.GraphQLServer.Schema.Queries
{
    public class QueryType : ObjectType
    {
        protected override void Configure(IObjectTypeDescriptor descriptor)
        {
            descriptor.Field(name: "restaurants")
                .Type<ListType<RestaurantType>>()
                .Resolver(async (ctx) =>
                {
                    var restaurantsService = ctx.Service<IRestaurantService>();
                    return await restaurantsService.GetRestaurantsAsync();
                });

            descriptor.Field(name: "restaurant")
                .Type<RestaurantType>()
                .Argument("id", x => x.Type<NonNullType<UuidType>>())
                .Resolver(async (ctx) =>
                {
                    var restaurantsService = ctx.Service<IRestaurantService>();
                    var restId = ctx.Argument<Guid>("id");
                    var restaurant = await restaurantsService.GetRestaurantAsync(restId);
                    return restaurant;
                });

            descriptor.Field(name: "table")
                .Type<TableType>()
                .Argument("id", x => x.Type<NonNullType<UuidType>>())
                .Resolver(async (ctx) =>
                {
                    var tableService = ctx.Service<ITableService>();
                    var tableId = ctx.Argument<Guid>("id");

                    var table = await tableService.GetTableAsync(tableId);
                    return table;
                });
        }
    }
}
