﻿using System;
using System.Threading.Tasks;
using AntFood.Contracts;
using AntFood.Contracts.Types;
using AntFood.Domain.Services;
using HotChocolate.Execution;

namespace AntFood.GraphQLServer.Schema.Mutations
{
    public class Mutation
    {
        private readonly IRestaurantService _restaurantService;
        private readonly ITableService _tableService;
        private readonly IFoodService _foodService;
        private readonly IOrderService _orderService;

        public Mutation(IRestaurantService restaurantService, 
            ITableService tableService, 
            IFoodService foodService,
            IOrderService orderService)
        {
            _restaurantService = restaurantService;
            _tableService = tableService;
            _foodService = foodService;
            _orderService = orderService;
        }

        public async Task<RestaurantType> AddRestaurant(string name)
        {
            return await _restaurantService.AddRestaurantAsync(name);
        }

        public async Task<TableType> AddTable(AddTableInput input)
        {
            return await _tableService.AddTableAsync(input);
        }

        public async Task<FoodType[]> AddFood(AddFoodInput[] input)
        {
            return await _foodService.AddFoodsAsync(input);
        }

        public async Task<OrderType> AddOrder(Guid tableId)
        {
            return await _orderService.AddOrderAsync(tableId);
        }

        public async Task<bool> AddOrderItems(Guid orderId, AddOrderItemsInput[] items)
        {
            return await _orderService.AddOrderItemsAsync(orderId, items);
        }
    }
}
