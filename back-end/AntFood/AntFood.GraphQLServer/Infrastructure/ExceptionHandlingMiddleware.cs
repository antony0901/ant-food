﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AntFood.Framework.Helpers;
using HotChocolate.Execution;
using HotChocolate.Resolvers;
using Microsoft.AspNetCore.Mvc.Filters;

namespace AntFood.GraphQLServer.Infrastructure
{
    public class ExceptionHandlingMiddleware
    {
        private readonly FieldDelegate _next;

        public ExceptionHandlingMiddleware(FieldDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(IMiddlewareContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                if(ex is DomainException)
                {
                    context.ReportError(ex.Message);
                    return;
                }

                throw;
            }
        }
    }
}
