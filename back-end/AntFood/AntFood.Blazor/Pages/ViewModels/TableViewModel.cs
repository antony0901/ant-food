﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AntFood.Blazor.Pages.ViewModels
{
    public class TableViewModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
