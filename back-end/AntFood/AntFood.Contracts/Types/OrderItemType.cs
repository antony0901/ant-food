﻿using System;

namespace AntFood.Contracts.Types
{
    public class OrderItemType
    {
        public Guid FoodId { get; set; }

        public string FoodName { get; set; }

        public int Quantity { get; set; }

        public decimal UnitPrice { get; set; }
    }
}
