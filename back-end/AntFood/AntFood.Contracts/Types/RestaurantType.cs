﻿using System;
using AntFood.Contracts.Enums;
using AntFood.Contracts.Types;

namespace AntFood.Contracts
{
    public class RestaurantType
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public Status Status { get; set; }

        public TableType[] Tables { get; set; }

        public MenuType[] Menus { get; set; }
    }
}
