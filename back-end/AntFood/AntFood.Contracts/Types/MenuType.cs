﻿using System;
using System.Collections.Generic;
using System.Text;
using AntFood.Contracts.Enums;

namespace AntFood.Contracts.Types
{
    public class MenuType
    {
        public FoodType[] Foods { get; set; }
        public MenuCategory Category { get; set; }
    }
}
