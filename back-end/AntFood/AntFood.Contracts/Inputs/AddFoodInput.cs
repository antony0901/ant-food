﻿using System;
using AntFood.Contracts.Enums;

namespace AntFood.Contracts
{
    public class AddFoodInput
    {
        public string Name { get; set; }

        public string ImageUrl { get; set; }

        public Guid RestaurantId { get; set; }
    }
}
