﻿using System;
using AntFood.Contracts.Enums;

namespace AntFood.Contracts
{
    public class AddOrderItemsInput
    {
        public Guid FoodId { get; set; }

        public int Quantity { get; set; }
    }
}
