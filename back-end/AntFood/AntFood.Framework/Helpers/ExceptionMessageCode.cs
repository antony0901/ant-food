﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AntFood.Framework.Helpers
{
    public static class ExceptionMessageCode
    {
        public static string TABLE_OCCUPIED = "TABLE_OCCUPIED";
        public static string NOT_FOUND = "NOT_FOUND";
    }
}
