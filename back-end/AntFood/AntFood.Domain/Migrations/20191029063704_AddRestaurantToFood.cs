﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AntFood.Domain.Migrations
{
    public partial class AddRestaurantToFood : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "FoodId",
                table: "Restaurants",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ImageUrl",
                table: "Foods",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Restaurants_FoodId",
                table: "Restaurants",
                column: "FoodId");

            migrationBuilder.AddForeignKey(
                name: "FK_Restaurants_Foods_FoodId",
                table: "Restaurants",
                column: "FoodId",
                principalTable: "Foods",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Restaurants_Foods_FoodId",
                table: "Restaurants");

            migrationBuilder.DropIndex(
                name: "IX_Restaurants_FoodId",
                table: "Restaurants");

            migrationBuilder.DropColumn(
                name: "FoodId",
                table: "Restaurants");

            migrationBuilder.DropColumn(
                name: "ImageUrl",
                table: "Foods");
        }
    }
}
