﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AntFood.Contracts;
using AntFood.Contracts.Types;

namespace AntFood.Domain.Services
{
    public interface IOrderService
    {
        Task<OrderType[]> GetOrdersAsync(Guid restaurantId);

        Task<OrderType> GetUnpaidOrdersByTableAsync(Guid tableId);

        Task<OrderType[]> GetPaidOrdersByTableIdAsync(Guid tableIds, DateTime? from, DateTime? to);

        Task<OrderType> AddOrderAsync(Guid tableId);

        Task<bool> AddOrderItemsAsync(Guid orderId, AddOrderItemsInput[] input);

        Task<OrderItemType[]> GetOrderItemsAsync(Guid orderId);
    }
}
