﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AntFood.Contracts;
using AntFood.Contracts.Enums;
using AntFood.Contracts.Types;

namespace AntFood.Domain.Services
{
    public interface IFoodService
    {
        Task<FoodType[]> AddFoodsAsync(AddFoodInput[] addFoodInput);

        Task<FoodType[]> GetAllFoodsAsync();

        Task<FoodType[]> GetFoodsAsync(Guid restaurantId);

        Task<MenuType[]> GetMenusAsync(Guid restaurantId);

        Task<MenuType> AddMenuAsync(Guid restaurantId, MenuCategory category, Guid[] foodIds);
    }
}
