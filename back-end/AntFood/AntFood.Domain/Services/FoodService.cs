﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AntFood.Contracts;
using AntFood.Contracts.Enums;
using AntFood.Contracts.Types;
using AntFood.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace AntFood.Domain.Services
{
    public class FoodService : ApplicationServiceBase, IFoodService
    {
        public FoodService(AFDbContext dbContext)
            : base(dbContext)
        {
        }

        public async Task<FoodType[]> AddFoodsAsync(AddFoodInput[] addFoodInput)
        {
            var rs = new List<FoodType>();
            foreach (var food in addFoodInput)
            {
                var newFood = new Food(food.Name, food.RestaurantId)
                {
                    ImageUrl = food.ImageUrl
                };

                await _dbContext.AddAsync<Food>(newFood);
                rs.Add(new FoodType
                {
                    Id = newFood.Id,
                    Name = food.Name,
                    ImageUrl = food.ImageUrl
                });
            }
            await _dbContext.SaveChangesAsync();

            return rs.ToArray();
        }

        public async Task<MenuType> AddMenuAsync(Guid restaurantId, MenuCategory category, Guid[] foodIds)
        {
            var newMenu = new Menu(restaurantId, category);
            foreach (var foodId in foodIds)
            {
                newMenu.AddFood(foodId);
            }

            _dbContext.Add(newMenu);
            await _dbContext.SaveChangesAsync();
            var foods = await _dbContext.Set<Food>().Where(x => foodIds.Contains(x.Id)).Select(x => new FoodType
            {
                Id = x.Id,
                ImageUrl = x.ImageUrl,
                Name = x.Name
            }).ToArrayAsync();

            return new MenuType
            {
                Category = category,
                Foods = foods
            };
        }

        public async Task<FoodType[]> GetAllFoodsAsync()
        {
            Expression<Func<Food, bool>> predicate = x => true;
            return await GetFoods(predicate);
        }

        public async Task<FoodType[]> GetFoodsAsync(Guid restaurantId)
        {
            Expression<Func<Food, bool>> predicate = x => x.RestaurantId == restaurantId;
            return await GetFoods(predicate);
        }

        public async Task<MenuType[]> GetMenusAsync(Guid restaurantId)
        {
            var menuGroups = await _dbContext.Set<MenuFood>()
                .Include(x => x.Menu)
                .Include(x => x.Food)
                .Where(x => x.Menu.RestaurantId == restaurantId)
                .GroupBy(x => x.Menu)
                .ToArrayAsync();

            var rs = new List<MenuType>();
            foreach (var group in menuGroups)
            {
                var foods = group.Key.MenuFoods.Select(x => new FoodType
                {
                    Id = x.FoodId,
                    Name = x.Food.Name,
                    ImageUrl = x.Food.ImageUrl
                }).ToArray();
                var menuType = new MenuType
                {
                    Foods = foods,
                    Category = group.Key.Category
                };
                rs.Add(menuType);
            }

            return rs.ToArray();
        }

        private async Task<FoodType[]> GetFoods(Expression<Func<Food, bool>> predicate)
        {
            return _dbContext.Set<Food>().Where(predicate)
                .Select(x => new FoodType
                {
                    Id = x.Id,
                    Name = x.Name,
                    ImageUrl = x.ImageUrl
                })
                .ToArray();
        }
    }
}
