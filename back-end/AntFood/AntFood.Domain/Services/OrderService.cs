﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AntFood.Contracts;
using AntFood.Contracts.Types;
using AntFood.Domain.Models;
using AntFood.Framework.Helpers;
using Microsoft.EntityFrameworkCore;

namespace AntFood.Domain.Services
{
    public class OrderService : ApplicationServiceBase, IOrderService
    {
        public OrderService(AFDbContext dbContext)
            : base(dbContext)
        {
        }

        public async Task<OrderType> AddOrderAsync(Guid tableId)
        {
            var isTableOccupied = await _dbContext.Set<Order>()
                .AnyAsync(x => x.TableId == tableId 
                && x.PaidStatus == Contracts.Enums.PaidStatus.Unpaid);
            if(isTableOccupied)
            {
                throw new DomainException(nameof(Table), ExceptionMessageCode.TABLE_OCCUPIED);
            }

            var restaurantId = _dbContext.Set<Table>().FirstOrDefault(x => x.Id == tableId).RestaurantId;
            var newOrder = new Order(restaurantId, tableId);
            await _dbContext.Set<Order>().AddAsync(newOrder);
            await _dbContext.SaveChangesAsync();

            return new OrderType
            {
                Id = newOrder.Id,
                PaidStatus = newOrder.PaidStatus,
                TotalPrice = newOrder.TotalPrice()
            };
        }

        public Task<OrderType[]> GetOrdersAsync(Guid restaurantId)
        {
            throw new NotImplementedException();
        }

        public async Task<OrderType[]> GetPaidOrdersByTableIdAsync(Guid tableIds, DateTime? from, DateTime? to)
        {
            Expression<Func<Order, bool>> predicate = x => x.TableId == tableIds && x.PaidStatus == Contracts.Enums.PaidStatus.Paid;
            if(from.HasValue)
            {
                predicate = predicate.And(x => x.CreatedTime >= from.Value);
            }
            if (to.HasValue)
            {
                predicate = predicate.And(x => x.CreatedTime <= to.Value);
            }

            var paidOrders = await _dbContext.Set<Order>()
               .Where(predicate)
               .Select(x => new OrderType
               {
                   Id = x.Id,
                   TotalPrice = x.TotalPrice(),
                   CreatedTime = x.CreatedTime,
                   PaidStatus = x.PaidStatus
               }).ToArrayAsync();

            return paidOrders;
        }

        public async Task<bool> AddOrderItemsAsync(Guid orderId, AddOrderItemsInput[] input)
        {
            var order = await _dbContext.Set<Order>().FirstOrDefaultAsync(x => x.Id == orderId);
            if(order == null)
            {
                throw new DomainException(nameof(Order), ExceptionMessageCode.NOT_FOUND);
            }

            var foodIds = input.Select(x => x.FoodId).ToArray();
            if (!foodIds.Any())
            {
                throw new DomainException(nameof(Food), ExceptionMessageCode.NOT_FOUND);
            }

            var foods = await _dbContext.Set<Food>().Where(x => foodIds.Contains(x.Id)).ToArrayAsync();
            var orderItems = input.Select(x => 
            {
                var food = foods.FirstOrDefault(f => f.Id == x.FoodId);
                var item = new OrderItem(orderId, x.FoodId, x.Quantity, food.UnitPrice);
                return item;
            }).ToArray();

            order.AddItems(orderItems);

            return await _dbContext.SaveChangesAsync() > 0;
        }

        public async Task<OrderType> GetUnpaidOrdersByTableAsync(Guid tableId)
        {
            var unpaidOrder = await _dbContext.Set<Order>()
                .FirstOrDefaultAsync(x => x.TableId == tableId && x.PaidStatus == Contracts.Enums.PaidStatus.Unpaid);
            if(unpaidOrder == null)
            {
                return null;
            }

            return new OrderType
            {
                Id = unpaidOrder.Id,
                TotalPrice = unpaidOrder.TotalPrice(),
                CreatedTime = unpaidOrder.CreatedTime,
                PaidStatus = unpaidOrder.PaidStatus
            };
        }

        public async Task<OrderItemType[]> GetOrderItemsAsync(Guid orderId)
        {
            var orderItems = await _dbContext.Set<OrderItem>().Where(x => x.OrderId == orderId)
                .Include(x => x.Food)
                .Select(x => new OrderItemType
                {
                    FoodId = x.FoodId,
                    FoodName = x.Food.Name,
                    Quantity = x.Quantity,
                    UnitPrice = x.UnitPrice
                }).ToArrayAsync();

            return orderItems;
        }
    }
}
