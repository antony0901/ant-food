﻿using System;
using System.Collections.Generic;
using System.Text;
using AntFood.Contracts.Enums;
using AntFood.Domain.Infrastructure;

namespace AntFood.Domain.Models
{
    public class MenuFood : ValueObject
    {
        public MenuFood()
        {
        }
        public MenuFood(Guid foodId, Guid menuId)
        {
            FoodId = foodId;
            MenuId = menuId;
        }

        public Food Food { get; set; }

        public Guid FoodId { get; set; }

        public Menu Menu { get; set; }

        public Guid MenuId { get; set; }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return FoodId;
            yield return Menu.Id;
        }
    }
}
