﻿using AntFood.Contracts.Enums;
using AntFood.Domain.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AntFood.Domain.Models
{
    public class Order : EntityBase
    {
        public Order()
        {
        }

        public Order(Guid restaurantId, Guid tableId)
        {
            RestaurantId = restaurantId;
            TableId = tableId;
            OrderItems = new List<OrderItem>();
            PaymentMethods = new List<PaymentMethod>();
            PaidStatus = PaidStatus.Unpaid;
        }

        public Guid RestaurantId { get; set; }

        public Restaurant Restaurant { get; set; }

        public Guid TableId { get; set; }

        public Table Table { get; set; }

        public List<OrderItem> OrderItems { get; private set; }

        public PaidStatus PaidStatus { get; set; }

        public List<PaymentMethod> PaymentMethods { get; private set; }

        public decimal TotalPrice()
        {
            if(OrderItems == null)
            {
                return 0;
            }

            return OrderItems.Sum(x => x.Quantity * x.UnitPrice);
        }

        public void AddItems(OrderItem[] orderItems)
        {
            if(OrderItems == null)
            {
                OrderItems = new List<OrderItem>();
            }

            OrderItems.Clear();
            foreach (var item in orderItems)
            {
                OrderItems.Add(item);
            }
        }
    }
}
