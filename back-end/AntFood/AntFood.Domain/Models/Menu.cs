﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AntFood.Contracts.Enums;
using AntFood.Domain.Infrastructure;

namespace AntFood.Domain.Models
{
    public class Menu : EntityBase
    {
        protected Menu()
        {
        }

        public Menu(Guid restaurantId, MenuCategory category)
        {
            RestaurantId = restaurantId;
            Category = category;
            MenuFoods = new List<MenuFood>();
        }

        public Restaurant Restaurant { get; set; }

        public Guid RestaurantId { get; set; }

        public List<MenuFood> MenuFoods { get; set; }

        public MenuCategory Category { get; set; }

        public void AddFood(Guid foodId)
        {
            var newMenuItem = new MenuFood(foodId, Id);
            if(MenuFoods.Any(x => x.Equals(newMenuItem)))
            {
                return;
            }

            MenuFoods.Add(newMenuItem);
        }

    }
}
