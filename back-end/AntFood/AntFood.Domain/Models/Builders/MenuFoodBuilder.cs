﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace AntFood.Domain.Models.Builders
{
    public class MenuFoodBuilder
    {
        public static void Build(ModelBuilder modelBuilder)
        {
            var entity = modelBuilder.Entity<MenuFood>().ToTable("MenuFoods");

            entity.HasKey(x => new { x.MenuId, x.FoodId });
        }
    }
}
