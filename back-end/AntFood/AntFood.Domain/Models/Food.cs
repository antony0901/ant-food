﻿using AntFood.Domain.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AntFood.Domain.Models
{
    public class Food : AggregateRootBase
    {
        protected Food()
        {
        }

        public Food(string name, Guid restaurantId)
            : base()
        {
            Name = name;
            RestaurantId = restaurantId;
        }

        public string Name { get; private set; }

        public string ImageUrl { get; set; }

        public Restaurant Restaurant { get; private set; }

        public Guid RestaurantId { get; private set; }

        public decimal UnitPrice { get; set; }
    }
}
