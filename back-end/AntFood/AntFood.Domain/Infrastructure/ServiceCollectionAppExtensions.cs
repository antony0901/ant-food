﻿using System;
using System.Collections.Generic;
using System.Text;
using AntFood.Domain.Services;
using Microsoft.Extensions.DependencyInjection;

namespace AntFood.Domain.Infrastructure
{
    public static class ServiceCollectionAppExtensions
    {
        public static void RegisterApplicationServices(this IServiceCollection services)
        {
            services.AddScoped<IRestaurantService, RestaurantService>();
            services.AddScoped<ITableService, TableService>();
            services.AddScoped<IFoodService, FoodService>();
            services.AddScoped<IOrderService, OrderService>();
        }
    }
}
