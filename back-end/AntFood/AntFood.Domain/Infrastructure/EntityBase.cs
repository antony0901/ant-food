﻿using System;
using System.Collections.Generic;
using System.Text;
using AntFood.Framework.Helpers;

namespace AntFood.Domain.Infrastructure
{
    public abstract class EntityBase
    {
        public EntityBase()
        {
            Id = Guid.NewGuid();
            CreatedTime = DateTimeHelpers.GetTodayUtcTime();
        }

        public Guid Id { get; private set; }

        public DateTime CreatedTime { get; private set; }
    }
}
